<?php

$LANG = array(

# Global
'TRANSLATED_BY'              => 'Crédito da tradución: <a href="http://xmgz.eu">Xosé M.</a>',
'LANGUAGE_NAME'              => 'Galego',
'LANGUAGE_ISO_CODE_2_LETTER' => 'gl',
# http://www.w3schools.com/tags/ref_language_codes.asp --> full list

################################################################################
# Header HTML infos for search engine and title in tab:
'PEPPERCARROT_VEGETABLE' => 'Pepper e Carrot', // Non traduzo os nomes porque ninguén me axudou :( Eu propuxen Pepa & Ciro
'Website_DESCRIPTION'    => 'Sitio web oficial de Pepper&amp;Carrot, un webcómic libre e aberto sobre Pepper, unha fada moza e o seu gato, Carrot. Viven nun universo de fantasía con pocións, máxia e criaturas.', // si, "fada" en lugar de "bruxa".
'SUBTITLE'               => 'o Webcomic Aberto, por David Revoy',

################################################################################
# Top menu website:
'HOMEPAGE'   => 'Inicio',
'WEBCOMICS'  => 'Webcomics',
'BLOG'       => 'Blog',
'PHILOSOPHY' => 'Ideario',
'CONTRIBUTE' => 'Contribuir',
'COMMUNITY'  => 'Comunidade',
'WIKI'       => 'Wiki',
'SOURCES'    => 'Fontes',
'AUTHOR'     => 'Autor',
'FOLLOW'     => 'Siga a Pepper&amp;Carrot en:',

################################################################################
# Top and bottom translation panel
'ADD_TRANSLATION' => 'engada unha tradución',
'CORRECTIONS'     => 'propoña correccións',

################################################################################
# Page: Homepage
'HOMEPAGE_BIG_TEXT'         => '
    Un webcomic libre e código aberto<br/>
    coa axuda directa dos seus patrocinadores<br/>
    para cambiar a industria da banda deseñada!<br/>
',
'HOMEPAGE_PATREON_INFO'     => 'Por só $1 por novo episodio, fágase patrocinador agora en Patreon',
'HOMEPAGE_MOREINFO_BUTTON'  => 'Máis información',
'HOMEPAGE_PATREON_BUTTON'   => 'Convértase en patrocinador de Pepper&amp;Carrot en Patreon',
'HOMEPAGE_PATREON_BOX'      => 'Convértase en patrocinador de Pepper&amp;Carrot en:',
'HOMEPAGE_LAST_EPISODE'     => 'Último episodio',
'HOMEPAGE_NEWS_UPDATE'      => 'Novas e actualizacións',
'HOMEPAGE_MOREPOSTS_BUTTON' => 'Máis artigos',
'HOMEPAGE_MAINSERVICE_LINK' => 'https://www.patreon.com/davidrevoy',
'HOMEPAGE_SUPPORTED_BY'     => 'axudado por patrocinio.',
'HOMEPAGE_ALTERNATIVES'     => 'Alternativas:',

################################################################################
# Page: Webcomics
'WEBCOMIC_EPISODE'              => 'Episodios do webcomic',
'WEBCOMIC_MAKINGOF'             => 'Cómo se fixo',
'WEBCOMIC_MAKINGOF_DESCRIPTION' => '
<p>O Cómo se fixo e os <a href="http://www.davidrevoy.com/categorie5/tutorials">Titoriais artísticos</a> son contidos extra patrocinados polos meus <a href="https://www.patreon.com/davidrevoy">heroes en Patreon</a></p>
',
'WEBCOMIC_ARTWORK'              => 'Galería artística',
'WEBCOMIC_SKETCHES'             => 'Esbozos',
'WEBCOMIC_OLDER_COMICS'         => 'Anteriores webcomic do mesmo autor',

################################################################################
# Page: Blog
# when contents are not English (no need to warn user the content is english only here):
#'LIMITATIONS' 	=> ' Content available in english only ',
'LIMITATIONS' => '',

################################################################################
# Page: Philosophy
'PHILOSOPHY_TOP'    => '
 <h2>Axudado por patrocinadores</h2>

    <p>O proxecto Pepper&amp;Carrot está financiado só por patrocinadores, desde todo o mundo.<br/>
    Cada patrocinador envía unha pequena cantidade de cartos por cada novo episodio e obtén aparecer nos créditos ao final de cada novo episodio.<br/>
    Grazas a este sistema, Pepper&amp;Carrot pode permanecer autónomo e non depender da publicidade ou contaminarse coa propaganda.</p>
',
'PHILOSOPHY_BOTTOM' => '
    <img alt="illustration representing patronage" src="data/images/static/2015-02-09_philosophy_01-support.jpg">

    <h2>100% libre, por sempre, sen valos de pagamento</h2>

    <p>Todo o contido que produzo sobre Pepper&amp;Carrot está en este sitio web, libre e dispoñible para todas as persoas.<br/>
    Respéctovos a todas por igual: con ou sen cartos. Os contidos extra que fago para os patrocinadores tamén se mostran aquí.<br/>
    Pepper&amp;Carrot nunca lle pedirá cartos ou que se suscriba para ter acceso ao novo contido.</p>

        <img alt="Illustration representing paywall" src="data/images/static/2015-02-09_philosophy_03-paywall.jpg">

    <h2>Código aberto e licenzas</h2>

    <p>Quero darlle a xente o dereito de compartir, utilizar, construír e incluso facer cartos a partir do traballo creado por min.<br/>
    Todas as páxinas, traballos artísticos e contido son creados con Software Libre e de fontes abertas en Linux, e todas as fontes están en este sitio web, menú &#39;Source&#39;<br/>
    Anímote a facer un uso comercial, traducións, fan-art, impresións, películas, video xogos, compartir e republicar.<br/>
    Só precisa dar o crédito axeitado a/os autores (artístas, correctores, tradutores implicados no traballo que quere utilizar), proporcionar unha ligazón a licenza, e indicar os cambios realizados si procede. Pode facelo de varios xeitos apropiados, pero non dun xeito que implique que os autores propoñen o que vostede fixo.</p>

    <div class="philobutton">
        <a href="https://creativecommons.org/licenses/by/4.0/" title="Para máis información, lea a Creative Commons Attribution 4.0">
            Licenza: Creative Commons Attribution 4.0
        </a>
    </div>

        <img alt="Illustration representing open-source media" src="data/images/static/2015-02-09_philosophy_04-open-source.jpg">

    <h2>Entretemento de Calidade para todas, en calquer lugar</h2>

    <p>Pepper&amp;Carrot é un webcomic de humor/comedia axeitado para todas as persoas, de calquer idade.<br/>
    Sen contidos adultos ou violentos. Libre e de fontes abertas, Pepper&amp;Carrot é orguioso exemplo do fantástica que pode ser a cultura libre.<br/>
    Céntrome moito na calidade, xa que a libre e aberto non significa afeccionado. Moi ao contrario.</p>

        <img alt="Illustration representing comic pages flying around the world" src="data/images/static/2015-02-09_philosophy_05-everyone.jpg">

    <h2>Cambiemos a industria da banda deseñada!</h2>

    <p>Sen intermediarios entre artista e audiencia vostede paga menos e eu benefíciome máis. Vostede axúdame directamente.<br/>
    Ningunha editora, distribuidora, equipo de publicidade ou moda de turno pode forzarme a cambiar Pepper&amp;Carrot para axustarme a súa visión do &#39; mercado&#39;.<br/>
    Por que non podería unha soa historia de éxito mudar toda unha industria en crise? Xa veremos…</p>

        <img alt="Illustration: comic book industry vs. Patreon support" src="data/images/static/2015-02-09_philosophy_06-industry-change.jpg">

    <div class="philobutton">
        <a class="h4" href="https://www.patreon.com/davidrevoy" title="For only $1 per new episode, become a patreon of Pepper&amp;Carrot">
            Axúdeme a aumentar a produción de Pepper&amp;Carrot.
        </a>
    </div>

    <p>Grazas por ler!<br/>
    –David Revoy</p>

 ',

################################################################################
# Page: Contribute
'CONTRIBUTE_TITLE'       => 'Contribuir',
'CONTRIBUTE_TOP'         => '
    <p>Thanks to the <a href="?static6/sources" title="Sources page">open sources</a> and the <a href="http://creativecommons.org/licenses/by/4.0/">
    creative commons license</a> you can contribute to Pepper&amp;Carrot in many ways:</p>
',
'CONTRIBUTE_DONATION'    => '
    <h2>Patronage, donations</h2>

    <p>I detail in the <a href="?static2/philosophy" title="open the Philosophy page">Philosophy page</a> almost everything concerning funding.<br/>
    It&#39;s easy to become the patron of Pepper&amp;Carrot for only $1 per new episode <a href="https://www.patreon.com/davidrevoy">on Patreon</a>.<br/>
    Patreon accepts credit cards from all around the world and also PayPal.<br/>
    </p>
',
'CONTRIBUTE_FANART'      => '
    <h2>Fan-art</h2>

    <p>Pepper&amp;Carrot is open to fan-art: drawings, scenarios, scupltures, 3D models, fan-fiction. Send them to me (<a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a>, or poke me on social networks) to appear in the fan-art gallery:</p>
',
'CONTRIBUTE_DERIVATIONS' => '
    <h2>Derivatives</h2>

    <p>Pepper&amp;Carrot can be adapted to many project or products, why not do your own or join an existing one?</p>
',

'CONTRIBUTE_TRANSLATION' => '
    <h2>Translations and corrections</h2>

    <p>Pepper&amp;Carrot website is designed to be multilingual and accept any language (including extinct ones, or fictive ones). The sources of this page are at your disposal for you to translate them. Check the <a href="?static14/documentation&page=010_Translate_the_comic">reference tutorial</a> for more information on how to add your translation.</p>
',

'CONTRIBUTE_SOCIAL'      => 'Social networks',

'CONTRIBUTE_PRESS'       => '
    <h2>Press</h2>

    <p>Be the publisher of Pepper&amp;Carrot! Write articles, create posts on websites, share and build a community on your favorite social networks. You can download the <a href="?static6/sources" title="Sources page">press kit</a> on the "sources" page.</p>
',

'CONTRIBUTE_OTHER'       => '
    <h2>Other ideas to contribute…</h2>

    <p>Everyone can contribute in different ways:<br/>
        <b>Developers:</b> Create an application to read Pepper&amp;Carrot on mobile device.<br/>
        <b>Musician:</b> Create music themes for Pepper&amp;Carrot.<br/>
        <b>Writer:</b> Propose new Pepper&amp;Carrot scenarios.<br/>
        <b>Journalist:</b> Talk about Pepper&amp;Carrot in traditionnal media (printed press, tv, etc.)<br/>
        <b>Printer:</b> Print posters or goodies with Pepper&amp;Carrot on it.<br/>
    </p>
',

'CONTRIBUTE_IRC'         => '
    <h2 id="irc">IRC channel</h2>

    <p>Chat and discuss about Pepper&amp;Carrot. I&#39;m around at French day time (nickname: deevad)<br/>
',

################################################################################
# Page: Sources
'SOURCES_TITLE'     => 'Sources',
'SOURCES_TOP'       => '
    <p><b>Welcome to the sources download center!</b><br/>
    Here you&#39;ll find the source files of Pepper&amp;Carrot and more.<br/>
    All digital painting files here are compatible with the latest version of <a href="https://krita.org/">Krita</a>.</p>
',
'SOURCES_BOTTOM'    => '
    <p>By downloading and working with those files you agree to respect the terms of the <br/>
    <a href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution license</a>.
    Check the README files in each project for more infos.</p>

    <p><b>Code repositories for translations, scripts, the website, graphism, etc.:</b><br/></p>
',
'SOURCE_COVER'      => 'Cover images',
'SOURCE_KRITA'      => 'Krita source files',
'SOURCE_TRANSLATOR' => 'Translator\'s pack',
'SOURCE_WEB'        => 'Web: <span class="sourceinfos">90ppi, lightweight jpg</span>',
'SOURCE_PRINT'      => 'Print: <span class="sourceinfos">300ppi, lossless quality</span>',
'SOURCE_MONTAGE'    => 'Montage: <span class="sourceinfos">all panels in a single page</span>',

################################################################################
# Page: Author
'AUTHOR_TITLE'                       => 'About David Revoy',
'AUTHOR_BIO'                         => '
    <p>Hi, my name is David Revoy and I&#39;m a French artist born in 1981. I&#39;m self-taught and passionate about drawing, painting, cats, computers, Gnu/Linux open-source culture, Internet, old school RPG video-games, old mangas and anime, traditional art, japanese culture, fantasy…<br/>
    <br/>
    After more than 10 years of freelance in digital painting, teaching, concept-art, illustrating and art-direction I decided to start my own project. I finally found a way to mix all my passions together, the result is Pepper&amp;Carrot.</p>

    <p>My portfolio:</p>
',
'AUTHOR_TODO_DREAM'                  => '
    <h3 id="dream">Dream TO-DO list</h3>

    <p>
    ☐ Give a talk in a Japanese comic convention about Pepper&amp;Carrot.<br/>
    ☐ Play with a gamepad to a Pepper&amp;Carrot video-game.<br/>
    ☑ <a href="http://www.peppercarrot.com/en/article457/dream-to-do-list-100-fan-arts"> Get a gallery of 100 fan-arts.</a><br/>
    ☑ <a href="http://www.peppercarrot.com/en/article376/dream-to-do-list-a-wikipedia-page">Get a Wikipedia page.</a><br/>
    ☑ <a href="http://www.peppercarrot.com/article387/cosplay-by-maria-and-ekaterina">Receive a photo of a Pepper cosplay.</a><br/>
    ☑ <a href="http://www.peppercarrot.com/article304/dream-to-do-list-a-cat-named-carrot">Receive a photo of a red cat named Carrot.</a><br/>
    ☑ <a href="http://www.peppercarrot.com/article302/peppercarrot-has-over-500-supporters">Be supported by 500 patreons.</a><br/>
    ☐ Reach episode 100!<br/>
    </p>
',

'AUTHOR_CARREER_TITLE'               => 'My carreer in 7 bubbles',
'AUTHOR_CARREER_BUBBLE_DESCRIPTIONS' => '<p></p>',

################################################################################
# Website General: Footer
'FOOTER_CONTENT' => '
    <p>Webcomic, artworks and texts are licensed under a <a href="https://creativecommons.org/licenses/by/4.0/" title="For more information, read the Creative Commons Attribution 4.0">Creative Commons Attribution 4.0</a> license, unless otherwise noted in the page.<br/>
    Attribution to "David Revoy, www.davidrevoy.com". Contact me: <a href="mailto:info@davidrevoy.com">info@davidrevoy.com</a> for more informations.</p>

    <p>Website powered by <a href="http://www.pluxml.org" title="PluXml">PluXml</a></p>
',

################################################################################
# various utils:
'UTIL_NEXT_EPISODE'     => 'episodio seguinte',
'UTIL_PREVIOUS_EPISODE' => 'episodio anterior',
'UTIL_EPISODE'          => 'episodio',
'UTIL_BACK_TO_GALLERY'  => 'voltar a galería',
'UTIL_MORE'             => 'máis',
'UTIL_PAGE'             => 'páxina',
'UTIL_BY'               => 'por',
'UTIL_LICENSE'          => 'licenza',
'UTIL_DOT'              => '.',
'FIRST'                 => 'primeiro',
'LAST'                  => 'último',
);

# vim: set et ts=4 sw=4 :
?>
