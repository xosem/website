<?php if(!defined('PLX_ROOT')) exit; ?>
<!-- Follow me -->
<div class="followcomic col sml-12 med-12 lrg-12">
  <br/>Follow Pepper&amp;Carrot?
  <div class="grid">

  <div class="col sml-4 med-4 lrg-4">
  <a class="followbutton twitter" href="https://twitter.com/davidrevoy">
  Follow David Revoy on<br/>
  <b>Twitter</b></span></a>
  </div>

  <div class="col sml-4 med-4 lrg-4">
  <a class="followbutton facebook " href="https://www.facebook.com/pages/Pepper-Carrot/307677876068903">
  Join Pepper&amp;Carrot<br/>
  fanpage on <b>Facebook</b>
  </a>  
  </div>

  <div class="col sml-4 med-4 lrg-4">
  <a class="followbutton googleplus" href="https://plus.google.com/u/0/110962949352937565678/">
  Add David Revoy to<br/>
  your circles on <b>Google+</b>
  </a>
  </div>

</div>
